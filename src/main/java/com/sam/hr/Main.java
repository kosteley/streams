package com.sam.hr;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

public class Main {



    public static void main(String[] args) {
	  var items = List.of(
	          new Item(new Item(null, 5), 10),
	          new Item(new Item(null, 5), 8),
	          new Item(new Item(null, 4), 12),
	          new Item(new Item(null, 2), 15),
	          new Item(new Item(null, 1), 7)
      );

	  Stream<Item> stream = null;
	  stream.filter(item -> item.y > 10).map(Main.Item::getX).filter(item -> item.y == 5);

    }

    @Getter
    @AllArgsConstructor
    public static final class Item {
        private final Item x;
        private final int y;

    }
}
